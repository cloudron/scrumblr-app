# Scrumblr Cloudron App

This repository contains the Cloudron app package source for [Scrumblr](http://scrumblr.ca)

## Installation

[![Install](https://cloudron.io/img/button.svg)](https://cloudron.io/button.html?app=ca.scrumblr.cloudronapp)

or using the [Cloudron command line tooling](https://cloudron.io/references/cli.html)

```
cloudron install --appstore-id ca.scrumblr.cloudronapp
```

## Building

The app package can be built using the [Cloudron command line tooling](https://cloudron.io/references/cli.html).

```
cd scrumblr-app

cloudron build
cloudron install
```

