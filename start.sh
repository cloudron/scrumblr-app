#!/bin/bash

set -eu -o pipefail

mkdir -p /run/scrumblr

cat > /run/scrumblr/config.json <<EOF
{
  "server": {
    "port": 8000
  },
  "redis": {
    "url": "${CLOUDRON_REDIS_URL}"
  }
}
EOF

export NODE_ENV=production

echo "==> start scrumblr"
exec /usr/local/bin/gosu cloudron:cloudron node /app/code/server.js
