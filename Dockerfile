FROM cloudron/base:3.2.0@sha256:ba1d566164a67c266782545ea9809dc611c4152e27686fd14060332dd88263ea

RUN mkdir -p /app/code /app/pkg
WORKDIR /app/code

# fixes nconf usage (https://github.com/aliasaria/scrumblr/pull/161)
RUN curl -L https://github.com/cloudron-io/scrumblr/tarball/05c39202e8ff6215d3c3ec1fe09decec6ac60f9f | tar -xz --strip-components 1 -f -
RUN npm install
# remove hardcoded GA code
RUN sed -i -e 's/UA-2069672-4//' /app/code/views/*.pug
RUN ln -sf /run/scrumblr/config.json /app/code/config.json

COPY start.sh /app/pkg/

CMD [ "/app/pkg/start.sh" ]
