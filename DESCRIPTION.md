This app packages scrumblr <upstream>cb1475d4f</upstream>

### Overview

It is a minimalistic simulation of a physical agile kanban board that supports real-time collaboration.

It does not come with any kind of user-management.
